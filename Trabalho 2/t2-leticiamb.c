#include <stdio.h>
#include <stdlib.h>

#define MAX 1000
#define BRANCO 0
#define CINZA 1
#define PRETO 2

// Estrutura da fila que armazenará os vértices a serem explorados
typedef struct no 
{
    int valor;
    struct no *prox;
} No_fila;


No_fila *insereElem(No_fila *fila, int x);
No_fila *removeElem(No_fila *fila);
void libera(No_fila *fila);


int main()
{

    // Matriz de adjacencia que representa o grafo
    int adjacencia[MAX][MAX];

    // Fila que armazena os vetores inexplorados
    No_fila *fila = NULL;

    // Vetor que armazena o estado de cada vertice, sendo os possiveis valores:
    // BRANCO se o vertice ainda não foi visitado
    // CINZA se foi visitado porém não explorado
    // PRETO se seus adjacentes já foram visitados
    int cor[MAX];

    // Vetor que armazena a distancia de cada vertice até o vertice 0
    // Uma distancia de -1 significa que não existe caminho entre os vertices
    int distancia[MAX];

    // Número de autores e de relações de co-autoria, respectivamente
    int n, m;

    // Vertice que está sendo explorado
    int explorando;

    // Inteiro que armazena a distancia maxima encontrada
    int max;

    // Inteiros auxiliares
    int i, j, x, y;


    // Recebe o 'n' inicial
	scanf("%d", &n);
    // Recebe o 'm' inicial
	scanf("%d", &m);


	// Laço principal
	while (n != 0 || m != 0) 
	{

        // Reinicia a matriz de adjacencia e os vetores de estado e distancia
        for (i = 0; i < n; i++) 
        {
            for (j = 0; j < n; j++) 
                adjacencia[i][j] = 0;
            
            cor[i] = BRANCO;
            distancia[i] = -1;
        }

		// Recebe as 'm' relações de co-autoria
		for (i = 0; i < m; i++) 
		{
			scanf("%d", &x);
            scanf("%d", &y);
            adjacencia[x][y] = 1;
            adjacencia[y][x] = 1;
		}

        // Define o vetor 0 como inexplorado e como 0 de distancia
        cor[0] = CINZA;
        distancia[0] = 0;

        // Insere o vertice 0 na fila de inexplorados
        fila = insereElem(fila, 0);

        // Explora os elementos da fila
        while (fila != NULL) 
        {

            explorando = fila->valor;

            for (i = 0; i < n; i++) {
                if (adjacencia[explorando][i] == 1) 
                {
                    if (cor[i] == BRANCO) {
                        distancia[i] = distancia[explorando] + 1;
                        cor[i] = CINZA;
                        fila = insereElem(fila, i);
                    }
                }
            }

            fila = removeElem(fila);
            cor[explorando] = PRETO;

        }

        // Encontra algum vértice que possui distancia infinita, ou o que
        // possui maior distancia até o vértice 0
        max = 0;
        for (i = 0; i < n; i++) 
        {
            if (distancia[i] == -1) 
            {
                max = -1;
                break;
            } else 	
           		 if (distancia[i] > max)
                     max = distancia[i];
            
        }

		// Imprime o resultado
        if (max == -1) 
            printf("infinito\n");
        else 
            printf("%d\n", max);
        

		// Recebe um novo 'n'
		scanf("%d", &n);
        // Recebe um novo 'm'
		scanf("%d", &m);
	}

	return 0;
}


No_fila *insereElem(No_fila *fila, int x) 
{
    No_fila *pnovo = (No_fila *)malloc(sizeof(No_fila));

    pnovo->valor = x;
    pnovo->prox = NULL;
    No_fila *aux = fila;

    if (fila == NULL) 
        fila = pnovo;
    else
    {
        while (aux->prox != NULL) 
            aux = aux->prox;
        
        aux->prox = pnovo;
	}

    return fila;
}

No_fila *removeElem(No_fila *fila) 
{
    No_fila *apaga;
	apaga = fila;
	fila = fila->prox;
	free(apaga);

	return fila;
}
