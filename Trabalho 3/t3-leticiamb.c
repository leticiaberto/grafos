#include <stdio.h>
#include <stdlib.h>

#define MAX 6000
#define EXPLORADO 1
#define INEXPLORADO 0

// Estrutura utilizada pela lista de adjacencia
typedef struct no {
    int v;
    struct no *prox;
} No_lista;


No_lista *insereElem(No_lista *lista, int x);
void libera(No_lista *lista);


// Lista de adjacencia que representa o grafo
No_lista *adjacencia[MAX];

// Número de vértices e de arestas, respectivamente
int n, m;

// Vetor que armazena o valor de cada vértice
int valor[MAX];

// Vetor que armazena o estado de cada vertice, indicando se eles foram
// explorados ou não
int estado[MAX];

// Vetor que armazena o caminho maximo encontrado a partir de certo vértice
int caminhoMaximo[MAX];

// Inteiro que armazena o valor do caminho máximo encontrado
int max;


int main()
{

    // Inteiros auxiliares
    int i, j, x, y, inicial;


    // Recebe o 'n' inicial
	scanf("%d", &n);
    // Recebe o 'm' inicial
	scanf("%d", &m);

	// Laço principal
	while (n != 0 || m != 0)
    {

        max = 0;

        // Reinicia o vetor de estado dos vértices e a lista de adjacencia
        for (i = 0; i < n; i++)
        {
            estado[i] = INEXPLORADO;
            adjacencia[i] = NULL;
        }

        // Recebe o valor de cada vertice
        for (i = 0; i < n; i++)
        {
            scanf("%d", &valor[i]);
        }

		// Recebe as 'm' relações entre os vértices
		for (i = 0; i < m; i++)
        {
			scanf("%d", &x);
            scanf("%d", &y);
            adjacencia[x] = insereElem(adjacencia[x], y);
		}

        // Encontra quais são os vértices que não são dependentes de nenhum outro
        // A partir desses, chama a recursão para encontrar o maior caminho
        if (n == 1)
        {
            max = valor[0];
        }
        else{
            for (i = 0; i < n; i++)
            {
                if (estado[i] == INEXPLORADO)
                {
                    inicial = 1;
                    for (j = 0; j < n; j++)
                    {
                        if (possuiElemento(adjacencia[j], i))
                        {
                            inicial = 0;
                            break;
                        }
                    }
                    if (inicial)
                    {
                        exploraVertice(adjacencia[i], i);
                    }
                }
            }
        }

		// Imprime o resultado
        printf("%d\n", max);

        for (i = 0; i < n; i++)
        {
            libera(adjacencia[i]);
        }

		// Recebe um novo 'n'
		scanf("%d", &n);
        // Recebe um novo 'm'
		scanf("%d", &m);
	}

	return 0;
}

int exploraVertice(No_lista *adjacentes, int vertice)
{

    No_lista *i = adjacentes;

    // Soma encontrada no vértice adjacente que está sendo analisado
    int somaAtual;
    // Maior soma encontrada nos vértices adjacentes
    int somaMax = 0;

    while (adjacentes != NULL)
    {
        // Se o vértice adjacente já foi explorado, não é necessário chamar
        // a recursão
        if (estado[adjacentes->v] == EXPLORADO)
        {
            somaAtual = caminhoMaximo[adjacentes->v];
        }
        else
        {
            // Se o vértice adjacente não foi explorado, chama a recursão
            // para faze-lo
            somaAtual = exploraVertice(adjacencia[adjacentes->v], adjacentes->v);
        }
        if (somaAtual > somaMax)
        {
            somaMax = somaAtual;
        }
        adjacentes = adjacentes->prox;
    }

    somaMax += valor[vertice];

    // Armazena o caminho maximo a partir do vértice atual, e muda seu estado
    caminhoMaximo[vertice] = somaMax;
    estado[vertice] = EXPLORADO;

    if (somaMax > max)
    {
        max = somaMax;
    }

    return somaMax;
}


No_lista *insereElem(No_lista *lista, int x)
{
    No_lista *pnovo = (No_lista *)malloc(sizeof(No_lista));

    pnovo->v = x;
    pnovo->prox = lista;
    lista = pnovo;

    return lista;
}

int possuiElemento(No_lista *lista, int valor)
{
    int flag = -1,pos = -1;
    while (lista != NULL) {
        pos++;
        if (lista->v == valor)
            return 1;
        lista = lista->prox;
    }

    return 0;
}

void libera(No_lista *lista)
{
    No_lista *apaga,*aux = lista;
	while (aux!= NULL)
	{
		apaga = aux;
		aux = aux->prox;
		free (apaga);
	}
}
