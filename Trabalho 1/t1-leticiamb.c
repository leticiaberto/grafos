#include <stdio.h>
#include <stdlib.h>

#define MAX 1000
int arcos;

typedef struct no {
    int info;
    struct no *prox;
} No;

typedef struct cab {
    struct no *proxcab;
} No_cabeca;

void inic_lista (No_cabeca p[], int tam) {
    int i;
    
    for (i = 0; i < tam; i++) 
        p[i].proxcab = NULL;
}

void imprimirLista(No_cabeca p[], int tam)
{
	int i;
	printf("%d %d\n",tam,arcos);
	for(i = 0; i < tam; i++)
	{
		No *aux = p[i].proxcab;
		//printf("%d ", aux->info);
		while(aux != NULL)
		{
			printf("%d ", aux->info);
			aux = aux->prox;	
		}
		printf("\n");
	}
}
int main ()
{

    int matriz[MAX][MAX],matriztran[MAX][MAX], tam, i, j;
    No_cabeca c[MAX];
    No *lista;
    
   	scanf ("%d", &tam);
    
    while(tam != 0)
    {
				
	   inic_lista(c, tam);
	   arcos = 0;
		for (i = 0; i < tam; i++)  
		    for (j = 0; j < tam; j++) 
		        scanf ("%d", &matriz[i][j]);
		
		//Cria matriz transposta        
		for (j = 0; j < tam; j++) 
		{ 
		    for (i = 0; i < tam; i++) 
		       matriztran[j][i] = matriz[i][j]; 
		       //printf("%d ",matriztran[j][i]);
		      //printf("\n");
		}
		      
		for (i = 0; i < tam; i++) 
		{
		    for (j = 0; j < tam; j++)
		    {
		        if (matriztran[i][j] == 1) 
		        {
		        	arcos++;
		            No *novo;
		            
		            novo = malloc (sizeof(No));
		            novo->info = j;
		            novo->prox = NULL;
		            
		            if(c[i].proxcab == NULL)
		            	c[i].proxcab = novo;
		            else
		            {
				        No *aux;
				        aux = c[i].proxcab;
				        while (aux->prox != NULL)  
				                aux = aux->prox;
				         		                
				        aux->prox = novo;
		           	}
		          }
		            
		        }
		    }
        	imprimirLista(c, tam);
        	scanf ("%d", &tam);
       }
 	return 0;
}
